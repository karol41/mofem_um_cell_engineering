Cell engineering module {#cell_engineering_readme}
========================

This module allows for numerical calculation of cell forces applied on a surface
by means of a higher order finite element approach based on an linear elasticity
theory and Betti's theorem. Only experimental data of displacements in X and Y
direction is required. (possible further extensions if needed) For more
instructions, tutorials visit [MoFEM
page](http://mofem.eng.gla.ac.uk/mofem/html/pages.html).

[Implementation Documentation](http://mofem.eng.gla.ac.uk/mofem/html/cell__forces_8cpp.html)

How to cite?
===========

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.439392.svg)](https://doi.org/10.5281/zenodo.439392)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.439395.svg)](https://doi.org/10.5281/zenodo.439395)

Install cell enginnering module module on your MoFEM build
=========================
The simplest way to start using this module is install [Docker](https://www.docker.com/) and pull pre-compiled module from the [Docker Hub](https://hub.docker.com/r/lorak41/cell_engineering/),
~~~~
docker pull likask/mofem_cell_engineering
docker run --name cell_engineering likask/mofem_cell_engineering
docker run --rm=true -it --volumes-from cell_engineering -v $HOME/mofem-cephas/mofem:/mofem -v $HOME:$HOME -e HOSTHOME=$HOME likask/mofem_cell_engineering /bin/bash
~~~~

You can install this module on your MoFEM build as well.
Go to your build (users modules)

~~~~~
cd $MOFEM_INSTALL_DIR/um/users_modules/users_modules/
git clone https://karol41@bitbucket.org/karol41/mofem_um_cell_engineering.git cell_engineering
cd ..

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS="-Wall"  \
-DCMAKE_CXX_FLAGS="-Wall -Wno-bind-to-temporary-copy -Wno-overloaded-virtual" \
-DCMAKE_EXE_LINKER_FLAGS="-L$MOFEM_INSTALL_DIR/local/lib" users_modules

cd cell_engineering
make
~~~~~

On Docker cmake command is slightly different:

~~~~~
cmake -DBUILD_SHARED_LIBS=yes -DCMAKE_BUILD_TYPE=Release users_modules
~~~~~

Entire installation process can be also found on the [Youtube](https://www.youtube.com/watch?v=6opfKER7JHA) video.

How does it work?
================

Lets consider body containing two different layers of materials. If we are in
possesion of displacement field (experimental data) occurring between those two
layers we can calculate forces and deformation on the top surface.

![Problem geometry](doc/figures/cell_layers.png)

In the first step, we need to map provided experimentally displacement field on finite element mesh.

![Cell forces](doc/figures/cell_engineering_forces_example.gif)

Raw data usually contains lots of noise and doesn't coincide with
the arrangement of triangles in the mesh. Therefore, least squares approximation
is used to produce smooth, continuous displacement field. Strength of smoothing
can be manipulated with different parameters. When generating the mesh in Cubit,
we need to remember to properly define material properties (MAT_ELASTIC
blockset) and indicate bottom (*101*) and top (*202*) surface of considered
layer as SIDESETS 101 and 202, respectively. Checkout the example .cub or .jou
files in /examples directory to see how it is done.

Check before run
================

* Check where is your mesh file
* Check where are your data XY (csv) files
* Check where is your config file (if required)

Displacements mapping
==========================
In order to map experimental data on finite element mesh we execute the following command.
```
./map_disp \
-my_data_x ./examples/data_x.csv \
-my_data_y ./examples/data_y.csv \
-my_file ./examples/mesh_cell_force_map.cub \
-ksp_type cg -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor \
-lambda 0.01 -my_order 4 -scale 0.05 -cube_size 1 -my_nparts 4
```
where

* loading data files, comma separated
* loading mesh
* solver settings
* lambda parameter drives "the strength" of approximation (the higher the values, more smoothed field we achieve)
* my_order sets order of approximation
* scale the distance between experimental data points (default: 1 unit)
* nparts set number of processors and partitioning mesh

If only one layer is specified, another thin polymer layer could be created
automatically using prism elements,
```
./map_disp_prism \
-my_thinckness 0.01 \
-my_data_x ./examples/data_x.csv \
-my_data_y ./examples/data_y.csv \
-my_file ./examples/mesh_for_prisms.cub \
-ksp_type cg -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor \
-lambda 0.01 -my_order 3 -scale 0.05 -cube_size 1 -my_nparts 4
```
where

* my_thinckness is a thickness of created layer on the top surface

Cell forces calculation
=========================
Code above produces output file (out.h5m) and analysis_mesh.h5m which can be subsequently used to calculate the required forces and stresses on the top surface of a given geometry.
```
mpirun -np 4 ./cell_forces \
-my_file analysis_mesh.h5m \
-my_order 1 -my_order_force 2 -my_max_post_proc_ref_level 1 \
-my_block_config ./examples/block_config.in \
-ksp_type fgmres -ksp_monitor \
-fieldsplit_1_ksp_type fgmres \
-fieldsplit_1_pc_type lu \
-fieldsplit_1_pc_factor_mat_solver_package mumps \
-fieldsplit_1_ksp_max_it 100 \
-fieldsplit_1_ksp_monitor  \
-fieldsplit_0_ksp_type gmres \
-fieldsplit_0_ksp_max_it 25 \
-fieldsplit_0_fieldsplit_0_ksp_type preonly \
-fieldsplit_0_fieldsplit_0_pc_type lu \
-fieldsplit_0_fieldsplit_0_pc_factor_mat_solver_package mumps \
-fieldsplit_0_fieldsplit_1_ksp_type preonly \
-fieldsplit_0_fieldsplit_1_pc_type lu \
-fieldsplit_0_fieldsplit_1_pc_factor_mat_solver_package mumps \
-ksp_atol 1e-6 -ksp_rtol 0 -my_eps_u 1e-4 -my_curl 1
```
where

* loading mesh
* solver settings
* my_order sets order of approximation
* block_config.in is a file where approximation order and material parameters could be set and changed

Do output VTK files and open them in Paraview
=========================

Stresses and displacements
~~~~~
mbconvert out.h5m out.vtk && open out.vtk
~~~~~
and to post-process tractions
~~~~~
mbconvert out_tractions.h5m out_tractions.vtk
~~~~~

Acknowledgements
========================
This user module has been co-developed with Biomedical Engineering and Infrastructure and Environment departments in the School of Engineering at the University of Glasgow.
